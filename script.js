"use strict";

$('.nav-tabs > li > a').click(function () {
    $(this).tab('show');
});

// accordions
var acc = document.getElementsByClassName("question-panel");

for (var i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");

    var more = this.nextElementSibling;
    if (more.style.display === "block") {
      more.style.display = "none";
    } else {
      more.style.display = "block";
    }
  });
}

function showHamburger() {
  document.querySelector('.navigation').classList.toggle('opened');
  document.querySelector('.nav-links').classList.toggle('show-ul');
}